seq-gen (1.3.4-3) unstable; urgency=medium

  * Team Upload.

  [ Steffen Möller ]
  * Update metadata with ref to conda

  [ Nilesh Patra ]
  * Add autopkgtests
  * Drop compat, switch to debhelper-compat 13
  * Add "Rules-Requires-Root:no"
  * Update metdata
  * Bump standards version to 4.5.0

  [ Andreas Tille ]
  * Add salsa-ci file (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 25 Jun 2020 20:33:35 +0530

seq-gen (1.3.4-2) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Thu, 11 Oct 2018 20:17:45 +0200

seq-gen (1.3.4-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata
    - Added references to registries
    - yamllint cleanliness

  [ Alexandre Mestiashvili ]
  * New upstream version 1.3.4
  * Refresh fix_typos.patch
  * Set XS-Autobuild to yes to force transition to testing,
    bump Standards-Version to 4.1.1
  * Drop lintian override for mangled upstream version

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Mon, 02 Oct 2017 16:35:44 +0200

seq-gen (1.3.3+git4a8cc9d.ds-1) unstable; urgency=medium

  * New upstream version 1.3.3+git4a8cc9d.ds
  * Refresh fix_typos.patch
  * Add d/README.source describing changes to the source
  * Add d/source/lintian.override,
    no need to mangle version in d/watch regularly

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Mon, 11 Sep 2017 16:23:47 +0200

seq-gen (1.3.3-2) unstable; urgency=medium

  [ Alexandre Mestiashvili ]
  * Update d/watch, get source from github, drop d/get-orig-source
  * New upstream version 1.3.3
  * Apply cme fix dpkg
  * Remove white lines and useles comments from d/rules
  * Update d/copyright, remove trailing white spaces, update copyright years
    remove Files-Excluded section, change BSD license to BSD-3-clause
  * Remove trailing white spaces in d/control
  * Update VCS-* fields in d/control to point to the new git repository
  * Introduce patch fixing makefile, enable hardening flags in d/rules
  * Add patch fixing a typo

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 07 Sep 2017 11:12:31 +0200

seq-gen (1.3.3-1) unstable; urgency=low

  [ Alexandre Mestiashvili ]
  * New upstream release 1.3.3
  * debian/control section changed to non-free/science
  * fixed debian/watch and debian/get-orig-source scripts
  * debian/seq-gen.1 fixed hyphen-used-as-minus-sign warning

  [ Andreas Tille ]
  * debian/control: Fixed Vcs fields
  * debian/upstream: Added citations
  * debhelper level 9 (control+compat)

 -- Andreas Tille <tille@debian.org>  Thu, 29 Mar 2012 13:30:11 +0200

seq-gen (1.3.2-2) unstable; urgency=low

  [ Alexandre Mestiashvili ]
  * fixed syntax-error-in-dep5-copyright warning

  [ Andreas Tille ]
  * Standards-Version: 3.9.2 (no changes needed)
  * debian/copyright:
    - License paragraph for packaging
    - License ist actually BSD license
  * Debhelper 8 (control+compat)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2011 22:47:16 +0200

seq-gen (1.3.2-1) unstable; urgency=low

  * Initial release (Closes: #617340)

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 03 Mar 2011 14:58:34 +0000
