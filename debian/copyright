Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: seq-gen
Source: https://github.com/rambaut/Seq-Gen

Files: *
Copyright: © Copyright (c)1996-2011, Andrew Rambaut & Nick Grassly
   Institute of Evolutionary Biology, University of Edinburgh
   All rights reserved.
License: BSD-3-clause

Files: source/eigen.*
Copyright: © Copyright (c)1993-2008, Ziheng Yang
License: PAML

Files: source/gamma.*
Copyright: © Copyright (c)1993-2008, Ziheng Yang
License: PAML

Files: source/twister.c
Copyright: © Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.
License: BSD-3-clause

Files: debian/*
Copyright: 2017 Alex Mestiashvili <alex@biotec.tu-dresden.de>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
   3. The names of its contributors may not be used to endorse or promote
      products derived from this software without specific prior written
      permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: PAML
 The software package is provided "as is" without warranty of any kind. In no
 event shall the author or his employer be held
 responsible for any damage resulting from the use of this software, including
 but not limited to the frustration that you may
 experience in using the package. The program package, including source codes,
 example data sets, executables, and this
 documentation, is distributed free of charge for academic use only. Permission
 is granted to copy and use programs in the
 package provided no fee is charged for it and provided that this copyright
 notice is not removed.
